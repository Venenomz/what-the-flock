﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node : MonoBehaviour
{
	public Node Previous;
	public Node Next;
	protected List<BaseJoint> joints = new List<BaseJoint>();

    public Vector3 myAccel;
    private Vector3 initAccel;
    public Vector3 prevPos;

    private readonly float grav = -9.82f;
    private readonly float fixedTimeStep = 0.033f; // unity's fixed time step value

	public Vector3 CurrentPosition
	{
		get
        {
			return transform.position;
		}
		set
        {
			if (Previous != null)
				transform.position = value;
		}
	}

	// Use this for initialization
	void Start ()
    {
		// does this node have no previous? if so then it is the root node so create the joints
		if (Previous == null)
        {
			CreateJoints();
		}
        // set current pos and acel
        prevPos = CurrentPosition;
        myAccel = new Vector3(0, grav, 0);
        initAccel = myAccel;
    }
	
	// Update is called once per frame
	void Update ()
    {
		// if this is the root node then tell the joints to update
		foreach(BaseJoint joint in joints)
        {
			joint.Update();
		}
	}

	void LateUpdate()
    {
        var curPos = CurrentPosition;

        // Verlet Integration
        var newPos = curPos + (curPos - prevPos) + myAccel * (fixedTimeStep * fixedTimeStep);

        prevPos = curPos;

        CurrentPosition = newPos;

        // reset accel so it doesn't keep scaling 
        myAccel = initAccel;
	}

	void CreateJoints()
    { 
		// traverse the chain of nodes
		Node current = this;
		while (current != null)
        {
			// if there is a next node then create the joint
			if (current.Next != null)
            {
				joints.Add(new BaseJoint(current, current.Next));
			}

			current = current.Next;
		}
	}
}
