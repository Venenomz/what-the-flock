﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseJoint
{
	public Node Node1;
	public Node Node2;

    private float initDist;
    private float curDist;

    Vector3 accel;

    readonly float sKon = -100f; // im not sure if i should make these positive or negitive numbers.
    readonly float dKon = -100f;

	public BaseJoint(Node _node1, Node _node2)
    {
		Node1 = _node1;
		Node2 = _node2;

        // set the initial dist between the two nodes
        initDist = (Node1.CurrentPosition - Node2.CurrentPosition).magnitude;
	}

	public virtual void Update()
    {
        // set cur dist
        curDist = (Node1.CurrentPosition - Node2.CurrentPosition).magnitude;

        // set acceleration
        accel = (-sKon) * (curDist - initDist) * (Node2.CurrentPosition - Node1.CurrentPosition).normalized;

        // Add node 1 acceleration to joint accel
        Node1.myAccel += accel;

        Node1.myAccel -= ((Node2.CurrentPosition - Node2.prevPos) - (Node1.CurrentPosition - Node1.prevPos)) * dKon;

        // Subtract node 2 acceleration from joint accel
        Node2.myAccel -= accel;

        Node2.myAccel += ((Node2.CurrentPosition - Node2.prevPos) - (Node1.CurrentPosition - Node1.prevPos)) * dKon;
	}
}
