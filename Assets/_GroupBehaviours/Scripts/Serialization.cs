﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.UI;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class Serialization : MonoBehaviour
{
    public List<Minion> minions;

    public MinionData data;

    [SerializeField]
    private Button save, load;

    private void Start()
    {
        // retreive all minions
        minions = new List<Minion>(FindObjectsOfType<Minion>());
    }

    private void OnEnable()
    {
        save.onClick.AddListener(SaveState);
        load.onClick.AddListener(LoadState);
    }

    private void OnDisable()
    {
        save.onClick.RemoveListener(SaveState);
        load.onClick.AddListener(LoadState);
    }

    private void SaveState()
    {
        // create a new file!
        var file = File.Create(Application.persistentDataPath + "/MinionData.dat");
        try
        {
            // create new data!
            var toSerialize = new DataToSerialize();

            // create a new formatter!
            var bf = new BinaryFormatter();

            toSerialize.minionDatas = new MinionData[minions.Count];

            // loop through all minions
            for (int i = 0; i < minions.Count; i++)
            {
                var curMinion = minions[i];

                // create new minion data of the current minion
                toSerialize.minionDatas[i] =
                    new MinionData(curMinion.transform.position.x,
                curMinion.transform.position.y,
                curMinion.transform.position.z,
                curMinion.transform.eulerAngles.x,
                curMinion.transform.eulerAngles.y,
                curMinion.transform.eulerAngles.z);
            }
            // serialize the current minions data into the file
            bf.Serialize(file, toSerialize);
        }
        catch (Exception e)
        {
            // catch the exception
            Debug.LogError(e);
        }
        finally
        {
            // close the file
            file.Close();
        }
    }

    private void LoadState()
    {
        // made it easier to debug
        bool exists = File.Exists(Application.persistentDataPath + "/MinionData.dat");

        // Find if the file exists.
        if (exists)
        {
            var fileStream = File.Open(Application.persistentDataPath + "/MinionData.dat", FileMode.Open);
            try
            {
                // create a new formatter
                var bf = new BinaryFormatter();

                //// close the file
                //fileStream.Close();

                // set the data to be the deserialized data
                DataToSerialize data = bf.Deserialize(fileStream) as DataToSerialize;

                // loop through minions
                for (int i = 0; i < minions.Count; i++)
                {
                    var curData = data.minionDatas[i];

                    // set position
                    minions[i].transform.position = new Vector3(curData.curX, curData.curY, curData.curZ);
                    // set rotation
                    minions[i].transform.rotation = Quaternion.Euler(curData.rotX, curData.rotY, curData.rotZ);
                }
            }
            catch (Exception e)
            {
                // catch the exception
                Debug.LogError(e);
            }
            finally
            {
                // close the stream
                fileStream.Close();
            }
        }
        else // there is no data
        {
            Debug.LogError("No previous save file.. Cannot load.");
            return;
        }
    }

    [System.Serializable]
    public class MinionData
    {
        public float curX, curY, curZ;

        public float rotX, rotY, rotZ;

        public MinionData(float _curX, float _curY, float _curZ, float _rotX, float _rotY, float _rotZ)
        {
            curX = _curX;
            curY = _curY;
            curZ = _curZ;
            rotX = _rotX;
            rotY = _rotY;
            rotZ = _rotZ;
        }
    }

    [System.Serializable]
    public class DataToSerialize
    {
        public MinionData[] minionDatas;
    }
}
