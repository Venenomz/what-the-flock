﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Minion : MonoBehaviour
{
	protected Vector3 velocity = Vector3.zero;

	public float Speed = 5f;

    public Vector3 curPos;
    public Vector3 curRotation;

	protected List<Minion> minions;

	// Define the priority of each flocking rule
	protected float CohesionStrength = 1.5f;
	protected float CohesionRange = 5f;

	protected float SeparationStrength = 1.5f;
	protected float SeparationDistance = 1f;

	protected float AlignmentStrength = 1f;
	protected float AlignmentRange = 5f;

	public virtual Vector3 Velocity
    {
		get
        {
			return velocity;
		}
		set
        {
			velocity = value;
		}
	}

	// Use this for initialization
	protected virtual void Start ()
    {
		// retreive all minions
		minions = new List<Minion>(FindObjectsOfType<Minion>());	
	}
	
	// Update is called once per frame
	protected virtual void Update ()
    {
		// Calculate and apply the flocking vector
		velocity = (velocity + Flocking_Update()).normalized * Speed;

		// Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + velocity, Color.white);

		// apply the velocity
		transform.position += velocity * Time.deltaTime;
	}

	protected Vector3 Flocking_Update()
    {
		// Calculate each of the vectors
		Vector3 cohesionVector = Flocking_CalculateCohesion(minions) * CohesionStrength;
		Vector3 separationVector = Flocking_CalculateSeparation(minions) * SeparationStrength;
		Vector3 alignmentVector = Flocking_CalculateAlignment(minions) * AlignmentStrength;

		// Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + cohesionVector, Color.red);
		// Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + separationVector, Color.green);
		// Debug.DrawLine(transform.position + Vector3.up, transform.position + Vector3.up + alignmentVector, Color.blue);

		// Calculate the flocking vector
		Vector3 flockingVector = cohesionVector + separationVector + alignmentVector;

		return flockingVector;
	}

	Vector3 Flocking_CalculateCohesion(List<Minion> flock)
    {
        var centerOfMass = Vector3.zero;

        foreach (var minion in flock)
        {
            // calculate the pos of all items
            centerOfMass += minion.transform.position;
        }
        centerOfMass /= flock.Count;

        var distanceFromCM = (centerOfMass - this.transform.position);

        return distanceFromCM.normalized;
	}
    Vector3 Flocking_CalculateSeparation(List<Minion> flock)
    {
        var seperation = Vector3.zero;

        foreach (var minion in flock)
        {
            // if the current minion is me, skip
            if (minion == this)
                continue;

            seperation += (this.transform.position - minion.transform.position);
        }

        seperation /= flock.Count - 1;

        return seperation.normalized;
	}

	Vector3 Flocking_CalculateAlignment(List<Minion> flock)
    {
        var averageVelocity = Vector3.zero;

        foreach (var minion in flock)
        {
            averageVelocity += minion.velocity;
        }

        averageVelocity /= flock.Count;

        return averageVelocity.normalized;
	}
}
